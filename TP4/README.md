# TP4 : Vers un réseau d'entreprise

## I. Dumb switch

### 3. Setup topologie 1

#### Commençons simple

- définissez les IPs statiques sur les deux VPCS

    ```sh
    PC1$ ip 10.1.1.1 255.255.255.0

    PC2$ ip 10.1.1.2 255.255.255.0
    ```

- `ping` un VPCS depuis l'autre

    ```sh
    PC1$ ping 10.1.1.2
    
    84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=3.634 ms
    84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=3.084 ms
    84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=4.083 ms
    84 bytes from 10.1.1.2 icmp_seq=4 ttl=64 time=3.258 ms
    ```

## II. VLAN

### 3. Setup topologie 2

#### Adressage

- définissez les IPs statiques sur tous les VPCS

    ```sh
    PC3$ ip 10.1.1.3/24 
    ```

- vérifiez avec des `ping` que tout le monde se ping

    ```sh
    PC1$ ping 10.1.1.2

    84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=3.739 ms
    84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=3.819 ms
    
    PC1$ ping 10.1.1.3

    84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=4.773 ms
    84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=7.185 ms
    
    PC2$ ping 10.1.1.1

    84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=6.257 ms
    84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=6.709 ms
    
    PC2$ ping 10.1.1.3

    84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=4.132 ms
    84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=1.819 ms
    
    PC3$ ping 10.1.1.1

    84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=6.173 ms
    84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=5.180 ms
    
    PC3$ ping 10.1.1.2

    84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=6.537 ms
    84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=6.334 ms
    ```
    
#### Configuration des VLANs

- déclaration des VLANs sur le switch `sw1`

    ```
    sw1$ enable
    sw1# conf t
    (config)# vlan 10
    (config-vlan)# name admins
    (config-vlan)# exit

    (config)# vlan 20
    (config-vlan)# name guests
    (config-vlan)# exit

    (config)# exit
    ```
    
- ajout des ports du switches dans le bon VLAN
    - ici, tous les ports sont en mode access : ils pointent vers des clients du réseau

        ```she
        sw1$ enable
        sw1# conf t
        (config)# interface Gi0/0
        (config-if)# switchport mode access
        (config-if)# switchport access vlan 10
        (config-if)# exit
        (config)# interface Gi0/1
        (config-if)# switchport mode access
        (config-if)# switchport access vlan 10
        (config-if)# exit
        (config)# interface Gi0/2
        (config-if)# switchport mode access
        (config-if)# switchport access vlan 20
        (config-if)# exit
        sw1# exit
        sw1$ show vlan br
        
        VLAN Name         Status    Ports
        ---- ------------ --------- ----------------------------
        1    default      active    Gi0/3, Gi1/0, Gi1/1, Gi1/2
                                    Gi1/3, Gi2/0, Gi2/1, Gi2/2
                                    Gi2/3, Gi3/0, Gi3/1, Gi3/2
                                    Gi3/3
        10   admins       active    Gi0/0, Gi0/1
        20   guests       active    Gi0/2
        ```
        
#### Vérif

- `pc1` et `pc2` doivent toujours pouvoir se ping

    ```sh
    PC1$ ping 10.1.1.2

    84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=16.232 ms
    84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=3.920 ms
    
    PC1$ ping 10.1.1.3

    host (10.1.1.3) not reachable
    
    
    
    PC2$ ping 10.1.1.1

    84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=7.890 ms
    84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=15.804 ms
    
    PC2$ ping 10.1.1.3

    host (10.1.1.3) not reachable
    ```

- `pc3` ne ping plus personne

    ```sh
    PC3$ ping 10.1.1.1

    host (10.1.1.1) not reachable

    PC3$ ping 10.1.1.2

    host (10.1.1.2) not reachable
    ```
    
## III. Routing

### 3. Setup topologie 3

#### Adressage

- définissez les IPs statiques sur toutes les machines sauf le routeur

    ```sh
    PC1$ ip 10.1.1.1/24
    PC2$ ip 10.1.1.2/24
    adm1$ ip 10.2.2.1/24
    web1$ sudo nano /etc/sysconfig/network-scripts/ifcfg-ens33
    ```
    ```bash=
    TYPE=Ethernet
    PROXY_METHOD=none
    BROWSER_ONLY=no
    BOOTPROTO=static
    DEFROUTE=yes
    IPV4_FAILURE_FATAL=no
    IPV6INIT=yes
    IPV6_AUTOCONF=yes
    IPV6_DEFROUTE=yes
    IPV6_FAILURE_FATAL=no
    NAME=ens33
    UUID=d1b47845-75d0-42bb-8a5a-b4f2155717e8
    DEVICE=ens33
    ONBOOT=yes
    IPADDR=10.3.3.1
    NETMASK=255.255.255.0
    ```
    
#### Configuration des VLANs

- déclaration des VLANs sur le switch `sw1`

    ```sh
    sw1$ enable
    sw1# conf t
    (config)# vlan 11
    (config-vlan)# name clients
    (config-vlan)# exit
    (config)# vlan 12
    (config-vlan)# name admins
    (config-vlan)# exit
    (config)# vlan 13
    (config-vlan)# name servers
    (config-vlan)# exit
    ```

- ajout des ports du switches dans le bon VLAN

    ```sh
    (config)#interface Gi0/0
    (config-if)#switchport mode access
    (config-if)#switchport access vlan 11
    (config-if)#exit
    (config)#interface Gi0/1
    (config-if)#switchport mode access
    (config-if)#switchport access vlan 11
    (config-if)#exit
    (config)#interface Gi0/2
    (config-if)#switchport mode access
    (config-if)#switchport access vlan 13
    (config-if)#exit
    (config)#interface Gi0/3
    (config-if)#switchport mode access
    (config-if)#switchport access vlan 12
    (config-if)#exit
    ```
    
- il faudra ajouter le port qui pointe vers le routeur comme un trunk : c'est un port entre deux équipements réseau (un switch et un routeur)

    ```sh
    sw1# conf t
    (config)# interface Gi1/0
    (config-if)# switchport trunk encapsulation dot1q
    (config-if)# switchport mode trunk
    (config-if)# switchport trunk allowed vlan add 11,12,13
    (config-if)# exit
    (config)# exit
    ```
    
#### Config du routeur

- attribuez ses IPs au routeur
    - 3 sous-interfaces, chacune avec son IP et un VLAN associé

        ```sh
        R1(config)#interface f0/0
        R1(config-subif)#no shut
        R1(config-subif)#
        R1(config)#interface f0/0.11
        R1(config-subif)#encapsulation dot1Q 11
        R1(config-subif)#ip addr 10.1.1.254 255.255.255.0
        R1(config-subif)#exit
        
        R1(config)#interface f0/0.12
        R1(config-subif)#encapsulation dot1Q 12
        R1(config-subif)#ip addr 10.2.2.254 255.255.255.0
        R1(config-subif)#exit
        
        R1(config)#interface f0/0.13
        R1(config-subif)#encapsulation dot1Q 13
        R1(config-subif)#ip addr 10.3.3.254 255.255.255.0
        R1(config-subif)#exit
        ```
        
#### Vérif

- tout le monde doit pouvoir ping le routeur sur l'IP qui est dans son réseau

    ```sh
    PC1$ ping 10.1.1.254

    84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=15.037 ms
    84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=16.128 ms
    
    
    
    PC2$ ping 10.1.1.254

    84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=24.314 ms
    84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=12.367 ms
    
    
    
    adm1$ ping 10.2.2.254

    84 bytes from 10.2.2.254 icmp_seq=1 ttl=255 time=23.596 ms
    84 bytes from 10.2.2.254 icmp_seq=2 ttl=255 time=23.968 ms
    
    
    
    web1$ ping 10.3.3.254
    
    64 bytes from 10.3.3.254: icmp_seq=1 ttl=255 time=22.8 ms
    64 bytes from 10.3.3.254: icmp_seq=2 ttl=255 time=18.5 ms
    ```

- en ajoutant une route vers les réseaux, ils peuvent se ping entre eux
    - ajoutez une route par défaut sur les VPCS

        ```sh
        PC1$  ip 10.1.1.1/24 10.1.1.254
        PC2$  ip 10.1.1.2/24 10.1.1.254
        adm1$ ip 10.2.2.1/24 10.2.2.254
        ```

    - ajoutez une route par défaut sur la machine virtuelle

        ```sh
        web1$ cat /etc/sysconfig/network
        
        GATEWAY=10.3.3.254
        ```
        
        On rajoute également des routes vers les autres VLANs
        
        ```sh
        web1$ cat /etc/sysconfig/network-scripts/route-ens33
        
        10.1.1.0/24 via 10.3.3.254 dev ens33
        10.2.2.0/24 via 10.3.3.254 dev ens33
        ```
        
    - testez des `ping` entre les réseaux

        ```sh
        PC1$ ping 10.2.2.1

        84 bytes from 10.2.2.1 icmp_seq=1 ttl=63 time=93.397 ms
        84 bytes from 10.2.2.1 icmp_seq=2 ttl=63 time=62.112 ms
        
        PC1$ ping 10.3.3.1

        84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=45.108 ms
        84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=39.068 ms
        ```
        
## IV. NAT

### 3. Setup topologie 4

#### Ajoutez le noeud Cloud à la topo

- côté routeur, il faudra récupérer un IP en DHCP

    ```sh
    R1# conf t
    R1(config)# interface f1/0
    R1(config-if)# ip address dhcp
    R1(config-if)# no shut
    R1(config-if)# exit
    R1(config)# exit
    R1# show ip int br
    
    Interface                  IP-Address      OK? Method Status                Protocol
    FastEthernet0/0            unassigned      YES NVRAM  up                    up
    FastEthernet0/0.11         10.1.1.254      YES NVRAM  up                    up
    FastEthernet0/0.12         10.2.2.254      YES NVRAM  up                    up
    FastEthernet0/0.13         10.3.3.254      YES NVRAM  up                    up
    FastEthernet1/0            192.168.37.138  YES DHCP   up                    up
    [...]
    ```
    
- vous devriez pouvoir `ping 1.1.1.1`

    ```sh
    R1#ping 1.1.1.1

    Type escape sequence to abort.
    Sending 5, 100-byte ICMP Echos to#####1.1.1.1, timeout is 2 seconds:
    !!!!!
    Success rate is 100 percent (5/5), round-trip min/avg/max = 64/65/68 ms
    ```
    
##### Configurez le NAT

```sh
R1# conf t
R1(config)#interface f0/0
R1(config-if)#ip nat inside
R1(config-if)#exit

R1(config)#interface f1/0
R1(config-if)#ip nat outside

*Mar  1 01:09:18.319: %LINEPROTO-5-UPDOWN: Line protocol on Interface NVI0, changed state to up
R1(config-if)#
*Mar  1 01:09:25.051: %SYS-3-CPUHOG: Task is running for (2036)msecs, more than (2000)msecs (0/0),process = Exec.
-Traceback= 0x612C9CD0 0x612CA974 0x61292050 0x61292310 0x61292434 0x61292434 0x61293304 0x612C6154 0x612D234C 0x612BC744 0x612BD3A8 0x612BE2F8 0x60F0A454 0x6040914C 0x60425410 0x604C8600
*Mar  1 01:09:25.491: %SYS-3-CPUYLD: Task ran for (2476)msecs, more than (2000)msecs (0/0),process = Exec
R1(config-if)# exit

R1(config)#access-list 1 permit any
R1(config)#ip nat inside source list 1 interface f1/0 overload
```

#### Test

- configurez l'utilisation d'un DNS
    - sur les VPCS

        ```sh
        PC1$  ip dns 8.8.8.8
        PC2$  ip dns 8.8.8.8
        adm1$ ip dns 8.8.8.8
        
        web1$ sudo nano /etc/resolv.conf
        
        nameserver 8.8.8.8
        ```
        
- vérifiez un ping vers un nom de domaine

    ```sh
    PC1$ ping google.com
    google.com resolved to 216.58.213.142

    84 bytes from 216.58.213.142 icmp_seq=1 ttl=127 time=31.703 ms
    84 bytes from 216.58.213.142 icmp_seq=2 ttl=127 time=44.533 ms
    ```
    
---

#### Esteban MARTINEZ - B2B
    
![cisco meme](https://memegenerator.net/img/instances/38438938/can-cisco-ios-no-no-shutdown.jpg)