# TP1 réseau

## I. Exploration locale en solo

### 1. Affichage d'informations sur la pile TCP/IP locale

#### En ligne de commande

##### Affichez les infos des cartes réseau de votre PC

Dans un terminal on peut retrouver ces informations avec la commande `ipconfig /all` ce qui nous donne : 

- Interface ethernet
```
Carte Ethernet Ethernet :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Killer E2500 Gigabit Ethernet Controller
   Adresse physique . . . . . . . . . . . : 08-97-98-9E-E0-FA
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui
```
- Interface WiFi
```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX200 160MHz
   Adresse physique . . . . . . . . . . . : 74-D8-3E-F2-CC-95
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::5834:3e5a:ad4d:d8a3%16(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.0.175(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   [...]
```
##### Affichez votre gateway

Toujours avec la commande `ipconfig /all` on retrouve cette ligne : 
```
[...]
Passerelle par défaut. . . . . . . . . : 10.33.3.253
[...]
```

#### En graphique (GUI : Graphical User Interface)

##### Trouvez comment afficher les informations sur une carte IP (change selon l'OS)

On trouve ces informations dans les paramètres : 

![](https://i.imgur.com/KhwgxBq.png)

#### Question

##### à quoi sert la gateway dans le réseau d'YNOV ?

La passerelle par défaut d'YNOV sert a faire communiquer le réseau d'YNOV avec d'autres réseaux via Internet.

### 2. Modifications des informations

#### A. Modification d'adresse IP (part 1)

##### Utilisez l'interface graphique de votre OS pour changer d'adresse IP : 

J'ai ici modifié mon adresse IP de base : `10.33.0.175` pour : `10.33.0.159`

![](https://i.imgur.com/HOB5gVt.png)

##### Il est possible que vous perdiez l'accès internet. Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.

Il est possible de perdre internet si on met une adresse IP déjà attribué car on va envoyer des paquets maison ne vas pas les recevoir parce que la paserelle préfèrera les envoyé à une machine en DHCP.

#### B. Table ARP

##### Exploration de la table ARP

- depuis la ligne de commande, afficher la table ARP

On peut afficher la table ARP avec la commande `arp /a` ce qui nous donne : 

```
Interface : 10.33.0.175 --- 0x10
  Adresse Internet      Adresse physique      Type
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  192.168.0.0           00-13-c6-00-02-0f     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.235.1 --- 0x12
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.17.1 --- 0x17
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```
- identifier l'adresse MAC de la passerelle de votre réseau, et expliquer comment vous avez repéré cette adresse MAC spécifiquement

On repère l'adresse MAC de la passerelle du réseau grâce à son IP (ici `10.33.3.253` come vu précédemment); l'adresse physique correspondante est `00-12-00-40-4c-bf`

##### Et si on remplissait un peu la table ?

- affichez votre table ARP

```
Interface : 10.33.0.175 --- 0x10
  Adresse Internet      Adresse physique      Type
  10.33.3.28            e4-5e-37-3a-65-fb     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  192.168.0.0           00-13-c6-00-02-0f     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  224.0.0.253           01-00-5e-00-00-fd     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```
- listez les adresses MAC associées aux adresses IP que vous avez ping

`10.33.3.28 . . . . . e4-5e-37-3a-65-fb`
`224.0.0.252 . . . . 01-00-5e-00-00-fc`
`224.0.0.253 . . . . 01-00-5e-00-00-fd`

#### C. nmap

##### Utilisez nmap pour scanner le réseau de votre carte WiFi et trouver une adresse IP libre

- lancez un scan de ping sur le réseau YNOV

Pour ce faire on utilise la commande `nmap -sP 10.33.0.0/22`

- affichez votre table ARP

```
> arp /a

Interface : 10.33.0.175 --- 0x10
  Adresse Internet      Adresse physique      Type
  10.33.0.27            a8-64-f1-8b-1d-4d     dynamique
  10.33.0.45            f2-4a-90-92-79-12     dynamique
  10.33.0.142           5c-3a-45-5f-48-5d     dynamique
  10.33.0.210           70-66-55-47-54-15     dynamique
  10.33.1.8             0c-dd-24-aa-e1-87     dynamique
  10.33.2.15            02-da-89-9f-5b-8c     dynamique
  10.33.2.209           5c-87-9c-e4-44-2c     dynamique
  10.33.3.15            26-09-60-6e-98-26     dynamique
  10.33.3.23            7e-7e-4e-bb-23-88     dynamique
  10.33.3.41            3c-58-c2-14-aa-5a     dynamique
  10.33.3.65            48-e7-da-69-32-77     dynamique
  10.33.3.168           12-88-c1-71-b7-a0     dynamique
  10.33.3.189           c2-6f-43-3d-c7-fa     dynamique
  10.33.3.207           c0-e4-34-1b-fe-a9     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  192.168.0.0           00-13-c6-00-02-0f     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique

Interface : 192.168.235.1 --- 0x12
  Adresse Internet      Adresse physique      Type
  192.168.235.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.17.1 --- 0x17
  Adresse Internet      Adresse physique      Type
  192.168.17.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

On constate ici que la liste est beaucoup plus longue.

#### D. Modification d'adresse IP (part 2)

##### Modifiez de nouveau votre adresse IP vers une adresse IP que vous savez libre grâce à nmap

- utilisez un ping scan sur le réseau YNOV
- montrez moi la commande nmap et son résultat

```
> nmap -sP 10.33.0.0/22

Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-13 17:30 Paris, Madrid (heure dÆÚtÚ)
Nmap scan report for 10.33.0.8
Host is up (0.050s latency).
MAC Address: EE:6B:A1:17:E3:B6 (Unknown)
Nmap scan report for 10.33.0.10
Host is up (0.51s latency).
MAC Address: B0:6F:E0:4C:CF:EA (Samsung Electronics)
[...]
Nmap scan report for 10.33.3.254
Host is up (0.010s latency).
MAC Address: 00:0E:C4:CD:74:F5 (Iskra Transmission d.d.)
Nmap scan report for 10.33.3.70
Host is up.
Nmap done: 1024 IP addresses (165 hosts up) scanned in 27.86 seconds
```

- prouvez en une suite de commande que vous avez une IP choisi manuellement, que votre passerelle est bien définie, et que vous avez un accès internet

Avec la commande `ipconfig /all` on peut voir que le DHCP est désactivé, qu'il y a bien une passerelle par défaut et avec la commande `ping 8.8.8.8` on voit qu'il y a bien une répone, ce qui signifie que je possède un accès internet.

```
[...]
    DHCP activé. . . . . . . . . . . . . . : Non
    Configuration automatique activée. . . : Oui
    Adresse IPv6 de liaison locale. . . . .: fe80::5834:3e5a:ad4d:d8a3%16(préféré)
    Adresse IPv4. . . . . . . . . . . . . .: 10.33.3.70(préféré)
    Masque de sous-réseau. . . . . . . . . : 255.255.252.0
    Passerelle par défaut. . . . . . . . . : 10.33.3.253
[...]
```
```
Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=21 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=17 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=18 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=17 ms TTL=115

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 17ms, Maximum = 21ms, Moyenne = 18ms
```
---

## II. Exploration locale en duo

### 3. Modification d'adresse IP

#### modifiez l'IP des deux machines pour qu'elles soient dans le même réseau

- utilisez ping pour tester la connectivité entre les deux machines

```
Envoi d’une requête 'Ping'  192.168.1.1 avec 32 octets de données :
Réponse de 192.168.1.1 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.1.1 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.1.1 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.1.1 : octets=32 temps=1 ms TTL=128
```

- affichez et consultez votre table ARP

Voici la table arp de l'interface ethernet : 

```
Interface : 192.168.1.2 --- 0x16
  Adresse Internet      Adresse physique      Type
  192.168.1.1           00-2b-67-fe-94-37     dynamique
  192.168.1.3           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```

### 4. Utilisation d'un des deux comme gateway

#### Sur le PC qui garde Internet

Après avoir configuré l'adresse IP, on active le partage via l'option de partage sur windows : 

![](https://i.imgur.com/SH0ydDc.png)

On test ensuite l'accès internet avec la commande `ping 1.1.1.1` : 

```
Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=18 ms TTL=58
Réponse de 1.1.1.1 : octets=32 temps=18 ms TTL=58
Réponse de 1.1.1.1 : octets=32 temps=18 ms TTL=58
Réponse de 1.1.1.1 : octets=32 temps=16 ms TTL=58
```

#### Sur le PC qui n'a pas internet

il suffit de configurer l'adresse IP et mettre la passerelle par défaut avec l'adresse IP de l'autre PC : 

```
   [...]
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.6(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . : 192.168.1.5
   [...]
```

-  utiliser un traceroute ou tracert pour bien voir que les requêtes passent par la passerelle choisie (l'autre le PC)

On retrouve bien l'itinéraire avec la commande `tracert -4 192.168.1.5`

```
Détermination de l’itinéraire vers Esteban-PC [192.168.1.5]
avec un maximum de 30 sauts :

  1     1 ms     1 ms     1 ms  Esteban-PC [192.168.1.5]

Itinéraire déterminé.
```

## III. Manipulations d'autres outils/protocoles côté client

(pour cette partie je ne suis pas sur le réseau d'YNOV)

### 2. DNS

#### trouver l'adresse IP du serveur DNS que connaît votre ordinateur

Avec la commande `ipconfig /all` on trouve la ligne correspondante au DNS : 

```
Serveurs DNS. . .  . . . . . . . . . . : 192.168.43.1
```

#### utiliser, en ligne de commande l'outil nslookup pour faire des requêtes DNS à la main

```
> nslookup google.com

Serveur :   UnKnown
Address:  192.168.43.1

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:818::200e
          142.250.74.238
          
          
          
> nslookup ynov.com

Serveur :   UnKnown
Address:  192.168.43.1

Réponse ne faisant pas autorité :
Nom :    ynov.com
Address:  92.243.16.143
```

- interpréter les résultats de ces commandes

On constate que la commande nous retourne en premier le serveur DNS par où est passé la requête (`Adress: 192.168.43.1`) ainsi que le nom de domaine recherché et l'IP correspondante (`google.com <-> 142.250.74.238` ou `ynov.com <-> 92.243.16.143`).

- déterminer l'adresse IP du serveur à qui vous venez d'effectuer ces requêtes

On en déduit que l'adresse est `192.168.43.1`

- faites un reverse lookup (= "dis moi si tu connais un nom de domaine pour telle IP")

    - pour l'adresse 78.74.21.21

		```
		> nslookup 78.74.21.21
		
		Serveur :   UnKnown
		Address:  192.168.43.1

		Nom :    host-78-74-21-21.homerun.telia.com
		Address:  78.74.21.21
		```
        
    - pour l'adresse 92.146.54.88

        ```
        Serveur :   UnKnown
		Address:  192.168.43.1

		Nom :    apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
		Address:  92.146.54.88
        ```
    - interpréter les résultats

        La commande nous retourne les noms de domaine correspondants aux adresses IP données (`host-78-74-21-21.homerun.telia.com <-> 78.74.21.21` et `apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr <-> 92.146.54.88`) ainsi que le serveur DNS par où est passé la requête (toujours `192.168.43.1`)
        
---

#### Esteban MARTINEZ - B2B