# TP3 : Progressons vers le réseau d'infrastructure

## I. (mini)Architecture réseau

### 1. Adressage

#### Tableau des réseaux : 

| Nom du réseau | Adresse du réseau | Masque            | Nombre de clients possibles | Adresse passerelle | Adresse broadcast |
|---------------|-------------------|-------------------|-----------------------------|--------------------|-------------------|
| `client1`     | `10.3.0.0/25`     | `255.255.255.128` | 128                         | `10.3.0.126`       | `10.3.0.127`      |
| `server1`     | `10.3.0.128/26`   | `255.255.255.192` | 64                          | `10.3.0.190`       | `10.3.0.191`      |
| `server2`     | `10.3.0.192/28`   | `255.255.255.240` | 16                          | `10.3.0.217`       | `10.3.0.218`      |



| Nom machine        | Adresse dans `client1` | Adresse dans `server1`| Adresse dans `server2` | Adresse de passerelle |
|--------------------|------------------------|-----------------------|------------------------|-----------------------|
|`router.tp3`        | `10.3.0.126/25`        | `10.3.0.190/26`       | `10.3.0.217/28`        | Carte NAT             |
|`dhcp.client1.tp3`  | `10.3.0.12/25`         |                       |                        | `10.3.0.126/25`       |
|`marcel.client1.tp3`| `10.3.0.13/25`         |                       |                        | `10.3.0.126/25`       |
|`dns1.server1.tp3`  |                        | `10.3.0.131/26`       |                        | `10.3.0.130/26`       |

### 2. Routeur

#### Vous pouvez d'ores-et-déjà créer le routeur. Pour celui-ci, vous me prouverez que : 

- il a bien une IP dans les 3 réseaux, l'IP que vous avez choisie comme IP de passerelle

    ```sh
    $ ip a
    
    [...]
    3: ens34: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 00:0c:29:95:ee:5f brd ff:ff:ff:ff:ff:ff
        inet 10.3.0.126/25 brd 10.3.0.127 scope global noprefixroute ens34
    [...]
    4: ens38: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 00:0c:29:95:ee:73 brd ff:ff:ff:ff:ff:ff
        inet 10.3.0.190/26 brd 10.3.0.191 scope global noprefixroute ens38
    [...]
    5: ens39: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 00:0c:29:95:ee:69 brd ff:ff:ff:ff:ff:ff
        inet 10.3.0.217/28 brd 10.3.0.223 scope global noprefixroute ens39
    [...]
    ```
    
- il a un accès internet

    ```sh
    $ ping 8.8.8.8
    
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=128 time=20.3 ms
    64 bytes from 8.8.8.8: icmp_seq=2 ttl=128 time=20.6 ms
    ```
    
- il a de la résolution de noms

    ```sh
    $ ping google.com
    
    PING google.com (142.250.179.110) 56(84) bytes of data.
    64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=1 ttl=128 time=18.3 ms
    64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=2 ttl=128 time=18.2 ms
    ```
    
- il porte le nom `router.tp3`

    ```sh
    $ hostname
    
    router.tp3
    ```
    
- n'oubliez pas d'activer le routage sur la machine

    ```sh
    $ sudo firewall-cmd --add-masquerade --zone=public
    $ sudo firewall-cmd --add-masquerade --zone=public --permanent
    ```
    
## II. Services d'infra

### 1. Serveur DHCP

#### Mettre en place une machine qui fera office de serveur DHCP dans le réseau `client1`.

(voir [TP2 - DHCP](https://gitlab.com/esteban47150/tp-reseaux-b2/-/tree/main/TP2#iii-dhcp))

📁 Fichier [`dhcpd.conf`](https://gitlab.com/esteban47150/tp-reseaux-b2/-/blob/main/TP3/files/dhcpd.conf)

#### Mettre en place un client dans le réseau `client1`

- la machine récupérera une IP dynamiquement grâce au serveur DHCP
- ainsi que sa passerelle et une adresse d'un DNS utilisable

    ```sh
    $ sudo nmcli connection modify ens33 ipv4.method auto
    $ sudo nmcli connection down ens33; sudo nmcli connection up ens33
    ```
    
#### Depuis marcel.client1.tp3

- prouver qu'il a un accès internet + résolution de noms, avec des infos récupérées par votre DHCP

    ```sh
    $ ping 8.8.8.8
    
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=20.9 ms
    64 bytes from 8.8.8.8: icmp_seq=2 ttl=127 time=20.6 ms
    [...]
    
    $ ping google.com
    
    PING google.com (142.250.75.238) 56(84) bytes of data.
    64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=1 ttl=127 time=17.9 ms
    64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=2 ttl=127 time=20.2 ms
    [...]
    ```
    
- à l'aide de la commande `traceroute`, prouver que `marcel.client1.tp3 `passe par `router.tp3` pour sortir de son réseau

    ```sh
    $ traceroute google.com
    
    traceroute to google.com (142.250.179.110), 30 hops max, 60 byte packets
        1  _gateway (10.3.0.126)  5.510 ms  5.292 ms  5.054 ms
    [...]
    ```
    
    > _gateway (10.3.0.126) [...]
    
### 2. Serveur DNS

#### B. SETUP copain

##### Mettre en place une machine qui fera office de serveur DNS

> ⚠️ **Suite à un problème technique, le réseau `10.3.0.192/28` changera pour `192.168.20.1/24`** ce qui nous donne : 

| Nom du réseau | Adresse du réseau | Masque            | Nombre de clients possibles | Adresse passerelle | Adresse broadcast |
|---------------|-------------------|-------------------|-----------------------------|--------------------|-------------------|
| `client1`     | `10.3.0.0/25`     | `255.255.255.128` | 128                         | `10.3.0.126`       | `10.3.0.127`      |
| `server1`     | `192.168.20.1/24` | `255.255.255.0`   | 256                         | `192.168.20.254`   | `192.168.20.255`  |
| `server2`     | `10.3.0.192/28`   | `255.255.255.240` | 16                          | `10.3.0.217`       | `10.3.0.218`      |

| Nom machine        | Adresse dans `client1` | Adresse dans `server1`| Adresse dans `server2` | Adresse de passerelle |
|--------------------|------------------------|-----------------------|------------------------|-----------------------|
|`router.tp3`        | `10.3.0.126/25`        | `10.3.0.190/26`       | `10.3.0.217/28`        | Carte NAT             |
|`dhcp.client1.tp3`  | `10.3.0.12/25`         |                       |                        | `10.3.0.126/25`       |
|`marcel.client1.tp3`| `10.3.0.13/25`         |                       |                        | `10.3.0.126/25`       |
|`dns1.server1.tp3`  |                        | `192.168.20.2/24`     |                        | `192.168.20.254/24`   |

- il faudra lui ajouter un serveur DNS public connu, afin qu'il soit capable de résoudre des noms publics comme `google.com`
    - conf classique avec le fichier `/etc/resolv.conf` ou les fichiers de conf d'interface`

        ```sh
        $ sudo nano /etc/resolv.conf
        
        1 | nameserver 1.1.1.1
        ```
        
- comme pour le DHCP, on part sur "rocky linux dns server" on Google pour les détails de l'install du serveur DNS
    - le paquet que vous allez installer devrait s'appeler `bind` : c'est le nom du serveur DNS le plus utilisé au monde

        ```sh
        $ sudo dnf install -y bind bind-utils
        ```
        
- il y aura plusieurs fichiers de conf : 
    - un fichier de conf principal named.conf

        ```shell
        $ sudo nano /etc/named.conf
        ```
        ```bash=
        //
        // named.conf
        //
        // Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
        // server as a caching only nameserver (as a localhost DNS resolver only).
        //
        // See /usr/share/doc/bind*/sample/ for example named configuration files.
        //

        options {
                listen-on port 53 { any; };
                listen-on-v6 port 53 { any; };
                directory       "/var/named";
                dump-file       "/var/named/data/cache_dump.db";
                statistics-file "/var/named/data/named_stats.txt";
                memstatistics-file "/var/named/data/named_mem_stats.txt";
                secroots-file   "/var/named/data/named.secroots";
                recursing-file  "/var/named/data/named.recursing";
                allow-query     { any; };

                [...]
                
                recursion yes;

                dnssec-enable yes;
                dnssec-validation yes;

                managed-keys-directory "/var/named/dynamic";

                pid-file "/run/named/named.pid";
                session-keyfile "/run/named/session.key";

                /* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
                include "/etc/crypto-policies/back-ends/bind.config";
        };

        logging {
                channel default_debug {
                        file "data/named.run";
                        severity dynamic;
                };
        };

        zone "server1.tp3" IN {
                type master;
                file "/var/named/server1.tp3.forward";
                allow-update { none; };
        };
        zone "server2.tp3" IN {
                type master;
                file "/var/named/server2.tp3.forward";
                allow-update { none; };
        };

        include "/etc/named.rfc1912.zones";
        include "/etc/named.root.key";
        ```
        
    - des fichiers de zone "forward"
        - permet d'indiquer une correspondance nom -> IP
        - un fichier par zone forward

            ```shell
            $ sudo nano /var/named/server1.tp3.forward
            ```
            ```bash=
            $TTL 86400
            @   IN  SOA     dns1.server1.tp3. root.server1.tp3. (
                     2021080804
                     3600
                     1800
                     604800
                     86400
            )
            @         IN  NS      dns1.server1.tp3.
            @         IN  A       192.168.20.2

            ; Set each IP address of a hostname. Sample A records.
            Dns1           IN  A       192.168.20.2
            router         IN  A       192.168.20.254
            ```
            ```shell
            $ sudo nano /var/named/server2.tp3.forward
            ```
            ```bash=
            $TTL 86400
            @   IN  SOA     dns1.server2.tp3. root.server2.tp3. (
                     2021080804
                     3600
                     1800
                     604800
                     86400
            )
            @         IN  NS      dns1.server2.tp3.
            @         IN  A       192.168.20.2

            ; Set each IP address of a hostname. Sample A records.
            Dns1           IN  A       192.168.20.2
            router         IN  A       192.168.20.254
            ```
            
##### Tester le DNS depuis marcel.client1.tp3

- définissez manuellement l'utilisation de votre serveur DNS
    ```sh
    $ sudo nano /etc/resolv.conf
    
    1 | nameserver 192.168.20.2
    ```
    ```sh
    $ sudo systemctl enable --now named
    $ sudo firewall-cmd --add-service=dns --permanent
    $ sudo firewall-cmd --reload
    ```
- essayez une résolution de nom avec dig
    - une résolution de nom classique
        - `dig <NOM>` pour obtenir l'IP associée à un nom

            ```sh
            $ dig google.com
            
            [...]
            ;; ANSWER SECTION:
            google.com.             300     IN      A       142.250.75.238
            [...]
            ;; SERVER: 192.168.20.2#53(192.168.20.2)
            [...]
            ```
- prouvez que c'est bien votre serveur DNS qui répond pour chaque `dig`
    ```sh
    $ dig ynov.com
    
    [...]
    ;; SERVER: 192.168.20.2#53(192.168.20.2)
    [...]
    ```
    
##### Configurez l'utilisation du serveur DNS sur TOUS vos noeuds

- les serveurs, on le fait à la main

    ```sh
    $ sudo nano /etc/resolv.conf
    
    nameserver 192.168.20.2
    ```
    
- les clients, c'est fait via DHCP

    ```sh
    esteban@dhcp$ sudo nano /etc/dhcp/dhcpd.conf
    ```
    ```bash=
    default-lease-time 900;
    max-lease-time 10800;
    ddns-update-style none;
    authoritative;
    subnet 10.3.0.0 netmask 255.255.255.128 {
      range 10.3.0.13 10.3.0.127;
      option routers 10.3.0.126;
      option subnet-mask 255.255.255.128;
      option domain-name-servers 192.168.20.2;
    }
    ```

    [📁 Fichiers named.conf](https://gitlab.com/esteban47150/tp-reseaux-b2/-/blob/main/TP3/files/named.conf)

    [📁 Fichiers de zone server1](https://gitlab.com/esteban47150/tp-reseaux-b2/-/blob/main/TP3/files/server1.tp3.forward)
    [📁 Fichiers de zone server2](https://gitlab.com/esteban47150/tp-reseaux-b2/-/blob/main/TP3/files/server2.tp3.forward)
